﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [SerializeField]
    private AudioSource source;

    [SerializeField]
    private List<KeyClip> clips;

    public bool IsPlaying => source.isPlaying;


    public void Play(string key)
    {
        var keyClip = clips.First(kc => kc.Key == key);
        source.clip = keyClip.Clip;
        source.pitch = Random.Range(0.8f, 1.1f);
        source.volume = keyClip.Volume;
        source.Play();
    }

    
}

[System.Serializable]
public class KeyClip : System.Object
{
    public string Key;
    public AudioClip Clip;
    [Range(0,1)]
    public float Volume;
}
