﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CandleTimer : MonoBehaviour
{
    [SerializeField]
    private float candleTotalLifeTime = 1200;

    [SerializeField]
    private Transform candleTransform;

    [SerializeField]
    private Renderer candleRenderer;

    [SerializeField]
    private Transform canfleFlame;

    [SerializeField]
    private AudioSource audio;


    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Shrink());
    }

    public void ResetShrink()
    {
        StopAllCoroutines();
        StartCoroutine(Shrink());
    }

    IEnumerator Shrink()
    {
        for (int t = 0; t < 1200; t += 1)
        {
            SetNewCandleScale(t);
            SetFlamePosition(t);
            var volume = 0.15f + t / 1200f;
            audio.volume = Mathf.Clamp(volume, 0, 1);
            yield return new WaitForSeconds(.1f);
        }

        SceneManager.LoadScene(0);
    }

    private void SetNewCandleScale(int step)
    {
        var newYScale = 1f - step * (0.9f / 1200);
        candleTransform.localScale = new Vector3(1, newYScale, 1);
    }

    private void SetFlamePosition(int step)
    {
        var size = candleRenderer.bounds.size;
        var staticOffset = 0.1f;
        var oldpos = canfleFlame.position;
        var newYPos = size.y + staticOffset;
        canfleFlame.position = new Vector3(oldpos.x, candleTransform.position.y + newYPos, oldpos.z);
    }
}
