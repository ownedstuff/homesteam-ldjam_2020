﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FirstPersonCamera : MonoBehaviour
{
    [SerializeField]
    private float mouseSensitivity = 100;

    [SerializeField]
    private Transform playerBody;

    [SerializeField]
    private PlayerController playerController;

    [SerializeField]
    private LayerMask interactibles;

    [SerializeField]
    private Image crosshair;

    [SerializeField]
    private TextMeshProUGUI itemNameText;
    
    private float xRotation = 0;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateClickStatus();
        UpdateLookAngles();
        UpdateCrossHair();
    }

    private void UpdateClickStatus()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Click();
        }
    }

    private void Click()
    {
        if (IsLookingAtInteractible(out RaycastHit hit))
        {
            var interactible = hit.collider.GetComponent<IInteractible>();
            interactible.Interact(playerController);
        }
    }

    private void UpdateLookAngles()
    {
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90, 90);

        transform.localRotation = Quaternion.Euler(xRotation, 0, 0);
        playerBody.Rotate(Vector3.up * mouseX);
    }

    private void UpdateCrossHair() 
    {
        if (IsLookingAtInteractible(out RaycastHit hit))
        {
            crosshair.enabled = true;
            var interactible = hit.collider.GetComponent<IInteractible>();
            itemNameText.text = interactible.GetName();
        }
        else 
        {
            crosshair.enabled = false;
            itemNameText.text = string.Empty;
        }
    }

    private bool IsLookingAtInteractible(out RaycastHit hit)
    {
        return Physics.Raycast(transform.position, transform.forward, out hit, 3, interactibles);
    }
}
