﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private CandleTimer candle;

    [SerializeField]
    private AudioManager audioManager;

    private List<Item> Inventory = new List<Item>();

    public void Pickup(Item item, string soundKey)
    {
        audioManager.Play(soundKey);
        if (item == Item.Candle)
        {
            candle.ResetShrink();
        }
        else
        {
            Inventory.Add(item);
        }

        if (Inventory.Contains(Item.OldPhotos) && Inventory.Contains(Item.BedtimeStories) && Inventory.Contains(Item.MomLocket))
        {
            SceneManager.LoadScene(0);
            Cursor.lockState = CursorLockMode.None;
        }
    }

    public bool HasItemInInventory(Item item)
    {
        return Inventory.Contains(item);
    }
}
