﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField]
    private CharacterController controller;

    [SerializeField]
    private Transform groudCheck;

    [SerializeField]
    private float groundDistance = 0.4f;

    [SerializeField]
    private LayerMask groundMask;

    [SerializeField]
    private AudioManager audioManager;

    private bool isGrounded;

    [SerializeField]
    private float speed = 2;

    private float gravity = -9.81f;

    Vector3 gravVelocity;
    // Update is called once per frame
    void Update()
    {
        isGrounded = Physics.CheckSphere(groudCheck.position, groundDistance, groundMask);

        if (isGrounded && gravVelocity.y < 0)
        {
            gravVelocity.y = -2f;
        }

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Move(x, z);
        CalculateGravity();
    }

    private void CalculateGravity()
    {
        gravVelocity.y += gravity * Time.deltaTime;
        controller.Move(gravVelocity * Time.deltaTime);
    }

    private void Move(float x, float z)
    {
        Vector3 move = transform.right * x + transform.forward * z;
        controller.Move(move * speed * Time.deltaTime);

        if (move != Vector3.zero && !audioManager.IsPlaying)
        {
            audioManager.Play(SoundKeys.Footstep);
        }
    }
}
