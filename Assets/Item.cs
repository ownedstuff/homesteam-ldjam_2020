﻿public enum Item
{
    None,
    Candle,
    DaddyScroll1,
    MomLocket,
    BedtimeStories,
    DaddyKey,
    OldPhotos,
    BasementKey,
    SerenaKey,
    DaddyScroll2,
    DaddyScroll3,
}

