﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour, IInteractible
{
    [SerializeField]
    private string Name;

    [SerializeField]
    private Item item;

    [SerializeField]
    private string soundKey;

    [SerializeField]
    private Transform player;

    public string GetName()
    {
        return Name;
    }

    void Update()
    {
        Vector3 targetPostition = new Vector3(player.position.x, transform.position.y, player.position.z);
        transform.LookAt(targetPostition);
    }

    public void Interact(PlayerController player)
    {
        player.Pickup(item, soundKey);
        Destroy(gameObject);
    }
}
