﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour, IInteractible
{
    [SerializeField]
    private Animator animator;

    [SerializeField]
    private Item requiredItem;

    [SerializeField]
    private string name;

    private string currentName;

    private bool isOpen;
    private bool isAnimating;

    public string GetName()
    {
        return currentName;
    }

    private void Start()
    {
        currentName = name;
    }

    public void Interact(PlayerController player)
    {
        if (requiredItem == Item.None)
        {
            DoInteraction();
        }
        else 
        {
            CheckIfPlayerCanInteract(player);
        }
    }

    private void DoInteraction()
    {
        isOpen = !isOpen;
        animator.SetTrigger(isOpen ? "Open" : "Close");
    }
    private void CheckIfPlayerCanInteract(PlayerController player)
    {
        if (player.HasItemInInventory(requiredItem))
        {
            DoInteraction();
        }
        else
        {
            DisplayLocked();   
        }
    }

    private void DisplayLocked()
    {
        currentName = "Locked";
        Invoke("ResetName", 5);
    }

    private void ResetName()
    {
        currentName = name;
    }
}
