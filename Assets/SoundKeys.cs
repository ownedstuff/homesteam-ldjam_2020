﻿public class SoundKeys
{
    public const string Footstep = "Footstep";
    public const string Candle = "Candle";
    public const string Scroll = "Scroll";
    public const string Key = "Key";
}
