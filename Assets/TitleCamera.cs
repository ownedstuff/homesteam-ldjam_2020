﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleCamera : MonoBehaviour
{
    // Update is called once per frame
    private float totalDegrees = 0;
    void Update()
    {
        var old = transform.rotation;

        totalDegrees += (180 / 150) * Time.deltaTime;
        var newY = Mathf.Lerp(0f, 180f, totalDegrees / 180);

        transform.localRotation = Quaternion.Euler(newY, old.y, newY);
    }
}
